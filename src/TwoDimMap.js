

class TwoDimMap {

  constructor() {
    this.cache_ = new Map();
  }


  add(coord, node) {
    const lng = coord[0];
    const lat = coord[1];

    let lngCache = this.cache_.get(lng);
    if (!lngCache) {
      lngCache = new Map();
      this.cache_.set(lng, lngCache);
    }

    lngCache.set(lat, node);
  }

  get(coord) {
    const lng = coord[0];
    const lat = coord[1];

    let lngCache = this.cache_.get(lng);
    if (lngCache) {
      return lngCache.get(lat);
    }
  }

  clear(coord) {
    const lng = coord[0];
    const lat = coord[1];

    let lngCache = this.cache_.get(lng);
    if (lngCache) {
      lngCache.delete(lat);
      if (lngCache.size === 0) {
        this.cache_.delete(lng);
      }
    }
  }


  flatten() {
    let flatArray = [];
    this.cache_.forEach(lngCache => {
      if (lngCache) {
        lngCache.forEach(node => {
          flatArray.push(node.value);
        });
      }
    });
    return flatArray;
  }

}

export default TwoDimMap;
