
class Chart {

  constructor() {
    mapboxgl.accessToken = 'pk.eyJ1IjoibGllYnJhbmQiLCJhIjoiY2lld2FpcXpkMTJpbjkxbTNzZGU1YXl6OCJ9.akW57lx-2jkyO6IohS6yfA';
    this.map = new mapboxgl.Map({
        container: 'map',
        style: 'mapbox://styles/mapbox/streets-v9',
        center: [-75, 39],
        zoom: 9
    });
    let mapboxResolver;
    this.mapboxReady = new Promise(resolve => {
      mapboxResolver = resolve;
    });
    this.map.on('load', mapboxResolver);
  }


  addPoly(id, json, color) {
    this.mapboxReady.then(() => {
      this.map.addLayer({
        id,
        'type': 'fill',
        'source': {
          'type': 'geojson',
          'data': json
        },
        'layout': {},
        'paint': { 'fill-color': color, 'fill-opacity': 0.3 }
        });
    });
  }

  addPoint(id, json, radius=5, color='#888') {
    this.mapboxReady.then(() => {
      this.map.addLayer({
        id,
        'type': 'circle',
        'source': {
          'type': 'geojson',
          'data': json
        },
        'layout': {},
        'paint': { 'circle-radius': radius, 'circle-color': color }
        });
    });
  }

  addLineString(id, json, color='#008') {
    this.mapboxReady.then(() => {
      this.map.addLayer({
        id,
        'type': 'line',
        'source': {
          'type': 'geojson',
          'data': json
        },
        'layout': {},
        'paint': { 'line-color': color, 'line-width': 3 }
        });
    });
  }

}

export default Chart;
