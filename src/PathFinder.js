import cheapRuler from 'cheap-ruler';
import { pointInPolygon } from './geojson-utils';

import Heap from './Heap';
import TwoDimMap from './TwoDimMap';

const STEP = 0.02;

class PathFinder {

  setStart(geoJSON) {
    this.ruler = cheapRuler(geoJSON.geometry.coordinates[1], 'meters');

    this.start = geoJSON.geometry;
  }


  setFinish(geoJSON) {
    this.finish = geoJSON.geometry;
  }


  addExclusionZone(geoJSON) {
    this.exclusion = geoJSON;
  }


  reset() {
    this.done = false;
    this.explored = new TwoDimMap();
    this.solution = [];
    this.frontier = new Heap();
    this.addToFrontier(this.start.coordinates, 0);
  }


  solve(chart, output) {
    output.innerText = 'STARTING PATH FINDER...' + this.start;
    this.startTime = Date.now();
    this.reset();

    while (!this.done) {
      this.iterate();
    }

    chart.addPoint('explored', GeoMultiPoint(this.explored.flatten()));
    chart.addLineString('route', GeoLineString(this.solution));
  }


  iterate() {
    this.current = this.frontier.extractMinimum();
    this.markAsExplored(this.current);

    if (this.checkDone(this.current)) return;

    const neighbours = this.getNeighbours(this.current.value);
    neighbours.forEach(n => {
      if (this.isExplored(n)) {
        return;
      }
      let cost;

      if (this.pointOnLand(n)) {
        cost = Infinity;
      } else {
        const stepCost = this.ruler.distance(this.current.value, n);
        cost = this.current.cost + stepCost;
      }

      const neighbourNode = this.addToFrontier(n, cost);
      neighbourNode.parent = this.current;
    });
  }


  checkDone(node) {
    if (Date.now() - this.startTime > (60 * 1000)) {
      output.innerText = 'TIMED OUT'
      this.done = true;
    }
    if (node.distanceToFinish < 1000) {
      output.innerText = 'PATH FOUND: ' + (Date.now() - this.startTime) + 'ms';
      this.done = true;
      this.buildSolution(node);
    }
    return this.done;
  }


  pointOnLand(coordinates) {
    const p = GeoPoint(coordinates).geometry;

    return (
      pointInPolygon(p, this.exclusion.features[0].geometry) ||
      pointInPolygon(p, this.exclusion.features[1].geometry));
  }


  addToFrontier(coordinates, cost) {
    const node = this.frontier.insert(cost, coordinates);
    node.distanceToFinish = this.ruler.distance(coordinates, this.finish.coordinates);
    node.cost = cost;
    return node;
  }


  markAsExplored(node) {
    this.explored.add(node.value, node);
  }


  isExplored(coord) {
    return this.explored.get(coord) !== undefined;
  }


  buildSolution(node) {
    this.solution = [];
    let iter = node;
    while (iter.parent) {
      this.solution.push(iter.value);
      iter = iter.parent;
    }
  }


  getNeighbours(node) {
    return [
      [fixRoundingError(node[0] - STEP), fixRoundingError(node[1] - STEP)],
      [fixRoundingError(node[0] - 0),    fixRoundingError(node[1] - STEP)],
      [fixRoundingError(node[0] + STEP), fixRoundingError(node[1] - STEP)],

      [fixRoundingError(node[0] - STEP), fixRoundingError(node[1])],
      [fixRoundingError(node[0] + STEP), fixRoundingError(node[1])],

      [fixRoundingError(node[0] - STEP), fixRoundingError(node[1] + STEP)],
      [fixRoundingError(node[0] - 0),    fixRoundingError(node[1] + STEP)],
      [fixRoundingError(node[0] + STEP), fixRoundingError(node[1] + STEP)],
    ]
  }

}


function fixRoundingError(val) {
  const R = 10000000;
  return Math.round(val*R)/R;
}

function GeoPoint(coordinates) {
  return {
    'type': 'Feature',
    'properties': {},
    'geometry': {
      'type': 'Point',
      coordinates
    }
  }
}

function GeoLineString(coordinates) {
  return {
    'type': 'Feature',
    'properties': {},
    'geometry': {
      'type': 'LineString',
      coordinates
    }
  }
}


function GeoMultiPoint(coordinates) {
  return {
    'type': 'Feature',
    'properties': {},
    'geometry': {
      'type': 'MultiPoint',
      coordinates
    }
  }
}


export default PathFinder;
