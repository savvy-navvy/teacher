
# Teacher

This repository contains effectively a code test around path finding algorithms.
In particular, it contains a solution to find a path from A to B, avoiding a
(generalised) land mass. But it contains several problems...


## Step one - identify the problems

The first task is to identify the problems with the existing code. You can simply
write your findings in a text file, email, or Google Doc. Explain what you believe
to be problems and/or inefficiencies in the code, and how you would (from a high
level) address them.

Goal: find at least 6 topics to discuss


## Step two - improve the performance

The second task is to improve the performance of the existing code. This is just
a test, so you do not need to worry about blocking/freezing the UX thread. The
challenge is purely to improve the performance of the algorithm (or come up with
a better one), not improve the UX.

See if you can

* find better data structures
* algorithm improvements
* JS improvements


Can you find faster routes from the different start/finish points (see commented
consts in App.js)

Bonus points

* What algorithm or changes would you propose to also find a route from New York
to Reeds Beach such that it goes through the Cape May canal?




# Getting set up

1. Fork this repository (if you want to share your solutions privately, we recommend forking it to a private repo and giving us access)
1. run
```
npm install
npm start
```
1. Now browse to http://127.0.0.1:4000/
1. Hit the 'FIND ROUTE' button in the top left
1. Wait for almost a minute (UX will freeze)
1. It should show dots the path finder explored, and a blue route to the finish
(if it times out - increase the timeout in PathFinder.js)

